# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false



#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-contacts

# Port 51002
server.port=51002

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMS (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- Redis Key name as action-requests:<moduleAbbr>:${channelSeqId} (e.g. action-requests:lms:10581856258-94299189, action-requests:ipsh:..., action-requests:cnr:....)
# 	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=CONT
# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# Redis Database connection details
Redis.DB.Url=localhost:6379
Redis.DB.Password=
# if SSL set to true, Redis client will use standard JRE truststore for SSL connectivity
Redis.DB.SSL.Enabled=false
# "true" indicates hostname mistmatch exception should be avoided (test env specific)
#Redis.DB.SSL.DisableHostNameVerification=false
## For Sentinel connection
#Redis.Sentinel.Enabled=true
#Redis.Sentinel.Urls=52.30.51.155:26379,34.242.88.42:26379,34.252.191.116:26379
#Redis.DB.Master.Name=redis-master
#Redis.DB.Password=redis-password
#Redis.Sentinel.FailoverWaitTimeMS=10000

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=CONT_ANY,CONTFILE_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. SweepStructure, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
###
RelRetryCfgIdentifier.CONT_ANY.payloadType=Contact
RelRetryCfgIdentifier.CONT_ANY.requestType=any
RelRetryCfgIdentifier.CONT_ANY.maxReleaseRetry=

RelRetryCfgIdentifier.CONTFILE_ANY.payloadType=ContactFile
RelRetryCfgIdentifier.CONTFILE_ANY.requestType=any
RelRetryCfgIdentifier.CONTFILE_ANY.maxReleaseRetry=




##################################################################
### Release Connector Specific Configuration
##################################################################
RelConnector.JMS_CONNECTOR.route=direct:JmsConnectorRoute


### Contact specific Transformer routes. This is used to transform payload. 
Connector.CONT_TXFMR.route=direct:TransformContactRoute


### Contact specific Backend Endpoints used to connect to backend (IPSH/CNR) to make release call.

Backend.ReleaseServiceUri.IPSH=t3://ec2-107-21-88-8.compute-1.amazonaws.com:10002
Backend.ReleaseServiceUri.CNR=http://localhost:51004

#BACKEND WEBLOGIC Initial Connection factory , provider url and JNDI connection factory name settings
BACKEND.JMS.InitialContextFactory=com.ibm.websphere.naming.WsnInitialContextFactory
BACKEND.JMS.ProviderUrl=${Backend.ReleaseServiceUri.IPSH}
BACKEND.JMS.QueueConnectionFactory=jms/stpjmsConnectionFactory

## jms queue details for releasing messages. These are commented out as on local we are using stub response.

RelConnector.CONT_BILLER_JMS.endpoint=custom-jms:queue:TXN_REL_API_REQ_QUEUE?disableReplyTo=true
RelConnector.CONT_EMPLOYEE_JMS.endpoint=custom-jms:queue:TXN_REL_API_REQ_QUEUE?disableReplyTo=true
RelConnector.CONT_VENDOR_JMS.endpoint=custom-jms:queue:TXN_REL_API_REQ_QUEUE?disableReplyTo=true
RelConnector.CONT_CUSTOMER_JMS.endpoint=custom-jms:queue:TXN_REL_API_REQ_QUEUE?disableReplyTo=true

## Stub response for release call. Comment these(121-124) and uncomment above(115-118) while deploying on any environment other then local.
#RelConnector.CONT_BILLER_JMS.endpoint=direct:ReleaseStub
#RelConnector.CONT_EMPLOYEE_JMS.endpoint=direct:ReleaseStub
#RelConnector.CONT_VENDOR_JMS.endpoint=direct:ReleaseStub
#RelConnector.CONT_CUSTOMER_JMS.endpoint=direct:ReleaseStub

### Routing Slips
# Contact specific
RelConnector.contact/biller.Any.routingSlip=${ValConnector.CONT_VAL.route},\
        ${Connector.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.CONT_BILLER_JMS.endpoint}

RelConnector.contact/employee.Any.routingSlip=${ValConnector.CONT_VAL.route},\
		${Connector.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.CONT_EMPLOYEE_JMS.endpoint}

RelConnector.contact/bene.Any.routingSlip=${ValConnector.CONT_VAL.route},\
		${Connector.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.CONT_VENDOR_JMS.endpoint}
		
RelConnector.contact/counter.Any.routingSlip=${ValConnector.CONT_VAL.route},\
        ${Connector.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.CONT_CUSTOMER_JMS.endpoint}

### Contact file specific Transformer routes. This is used to transform payload. 
RelConnectorFile.CONT_TXFMR.route=direct:TransformContactFileReleaseRoute

RelConnectorFile.CONT_BILLER_JMS.endpoint=custom-jms:queue:CBX_FILEAUTHRESPONSE?disableReplyTo=true
RelConnectorFile.CONT_EMPLOYEE_JMS.endpoint=custom-jms:queue:CBX_FILEAUTHRESPONSE?disableReplyTo=true
RelConnectorFile.CONT_VENDOR_JMS.endpoint=custom-jms:queue:CBX_FILEAUTHRESPONSE?disableReplyTo=true
RelConnectorFile.CONT_CUSTOMER_JMS.endpoint=custom-jms:queue:CBX_FILEAUTHRESPONSE?disableReplyTo=true
RelConnectorFile.CONT_ANY_JMS.endpoint=custom-jms:queue:CBX_FILEAUTHRESPONSE?disableReplyTo=true


### Routing Slips
# Contact File specific
RelConnectorFile.contact/employee.Any.routingSlip=${RelConnectorFile.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnectorFile.CONT_EMPLOYEE_JMS.endpoint}

RelConnectorFile.contact/bene.Any.routingSlip=${RelConnectorFile.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnectorFile.CONT_VENDOR_JMS.endpoint}
		
RelConnectorFile.contact/counter.Any.routingSlip=${RelConnectorFile.CONT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnectorFile.CONT_CUSTOMER_JMS.endpoint}

RelConnectorFile.contact/biller.Any.routingSlip=${RelConnectorFile.CONT_TXFMR.route},\
        ${RelConnector.JMS_CONNECTOR.route},\
		${RelConnectorFile.CONT_BILLER_JMS.endpoint}

RelConnectorFile.contact/any.Any.routingSlip=${RelConnectorFile.CONT_TXFMR.route},\
        ${RelConnector.JMS_CONNECTOR.route},\
		${RelConnectorFile.CONT_ANY_JMS.endpoint}

		
##################################################################
### Validation specific Connector routes
##################################################################
ValConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute

### Backend Endpoints
# CONT specific
# Validation Endpoint using Interface approach
Backend.ValidateServiceUri.IPSH=http://103.231.209.251:10006
#Backend.ValidateServiceUri.CNR=http://localhost:51004

## Validate endpoint details. These are commented out as on local we are using stub response.
ValConnector.CONT_VAL.route=bean:validateContactPayloadBean?method=validateRequestPayload
#ValConnector.CONT_BILLER_HTTP.endpoint=jetty://${Backend.ValidateServiceUri.IPSH}/IPSH_STP_WEB-1.0.0/paymentsAPI/v1/validators/cbxbeni
ValConnector.CONT_EMPLOYEE_HTTP.endpoint=jetty://${Backend.ValidateServiceUri.IPSH}/ipshstp/paymentsAPI/v1/validators/cbxbeni
ValConnector.CONT_VENDOR_HTTP.endpoint=jetty://${Backend.ValidateServiceUri.IPSH}/ipshstp/paymentsAPI/v1/validators/cbxbeni
#ValConnector.CONT_CUSTOMER_HTTP.endpoint=jetty://${Backend.ValidateServiceUri.CNR}/stubs-local/validate/v1/generic

ValConnector.CONT_BILLER_HTTP.endpoint=direct:UnsupportedValidateStub
#ValConnector.CONT_EMPLOYEE_HTTP.endpoint=direct:ValidateStub
#ValConnector.CONT_VENDOR_HTTP.endpoint=direct:ValidateStub
ValConnector.CONT_CUSTOMER_HTTP.endpoint=direct:UnsupportedValidateStub

### Routing Slips
# CONT specific
ValConnector.contact/biller.Any.routingSlip=${ValConnector.CONT_VAL.route},\
		${Connector.CONT_TXFMR.route},\
		${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnector.CONT_BILLER_HTTP.endpoint}

ValConnector.contact/employee.Any.routingSlip=${ValConnector.CONT_VAL.route},\
		${Connector.CONT_TXFMR.route},\
		${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnector.CONT_EMPLOYEE_HTTP.endpoint}

ValConnector.contact/bene.Any.routingSlip=${ValConnector.CONT_VAL.route},\
        ${Connector.CONT_TXFMR.route},\
		${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnector.CONT_VENDOR_HTTP.endpoint}
		
ValConnector.contact/counter.Any.routingSlip=${ValConnector.CONT_VAL.route},\
        ${Connector.CONT_TXFMR.route},\
		${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnector.CONT_CUSTOMER_HTTP.endpoint}

ValConnectorFile.CONT_BILLER_HTTP.endpoint=direct:ValidateStub
ValConnectorFile.CONT_EMPLOYEE_HTTP.endpoint=direct:ValidateStub
ValConnectorFile.CONT_VENDOR_HTTP.endpoint=direct:ValidateStub
ValConnectorFile.CONT_CUSTOMER_HTTP.endpoint=direct:ValidateStub
ValConnectorFile.CONT_ANY_HTTP.endpoint=direct:ValidateStub

### Routing Slips
# COntact File specific
ValConnectorFile.contact/biller.Any.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnectorFile.CONT_BILLER_HTTP.endpoint}

ValConnectorFile.contact/employee.Any.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnectorFile.CONT_EMPLOYEE_HTTP.endpoint}

ValConnectorFile.contact/bene.Any.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnectorFile.CONT_VENDOR_HTTP.endpoint}
		
ValConnectorFile.contact/counter.Any.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnectorFile.CONT_CUSTOMER_HTTP.endpoint}

ValConnectorFile.contact/any.Any.routingSlip=${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnectorFile.CONT_ANY_HTTP.endpoint}

##################################################################
### Backend Instance specific configuration for Camel Component
##################################################################

# Message Broker which is to be used by Commons for Release Batch Triggers, State Update Events and Event Publisher
# Supported value - RabbitMQ
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=localhost
Digital.RabbitMQ.Port=5672
Digital.RabbitMQ.User=guest
Digital.RabbitMQ.Password=guest
Digital.RabbitMQ.VHost=/

#consumer details for RMQ
Digital.RabbitMQ.ExchgEvent=cbxevents
Digital.RabbitMQ.RejectQueue=reject-contact-file-state-event
Digital.RabbitMQ.RejectQ.RoutingKey=business.*.*.*.plain
igital.RabbitMQ.RejectQ.PrefetchEnabled=true
Digital.RabbitMQ.RejectQ.PrefetchCount=30
# if SSL set to true, RMQ client will use standard JRE truststore for SSL connectivity.
# internally creates rmqTrustManager bean as default TrustManager for Camel producer/consumer endpoints
Digital.RabbitMQ.SSL.Enabled=false


#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
#  Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
RelTriggerHandler.CamelComponent=rabbitmq

# Comma separated list of Payload Types, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=Contact,ContactFile


#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
StateUpdHandler.CamelComponent=rabbitmq

#################################################################################################
###### Event Publish Handler specific Configuration
#################################################################################################

# Name of the Apache Camel Component,
# which is to be used by Commons package for publishing Events / Triggers
MsgPubHandler.CamelComponent=rabbitmq

# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=

# Represents service key regex pattern (product/subproduct), this module is supposed to process events/data related to
Digital.ServiceKey.Patterns=contact/bene:.*,contact/employee:.*,contact/counter:.*,contact/biller:.*,contact/any:upload

RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=Contact,ContactFile

# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400

##################################################################
### Zipkin specific options. These are helpful in tracing the request 
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411

#############################################################################################
###### Registering mgmt endpoints to Eureka
#############################################################################################
eureka.instance.statusPageUrlPath=${server.contextPath}${management.context-path}/info
eureka.instance.healthCheckUrlPath=${server.contextPath}${management.context-path}/health

##################################################################
### Reject specific Connector routes
##################################################################
RejConnectorFile.JMS_CONNECTOR.route=direct:JmsConnectorRoute

### Transformer routes for reject event.
RejConnectorFile.CONT_TXFMR.route=direct:TransformContactFileRejectRoute


# Endpoint using JMS
RejConnectorFile.CONT_REJECT.endpoint=custom-jms:queue:CBX_FILEAUTHRESPONSE?disableReplyTo=true
#RejConnectorFile.CONT_REJECT.endpoint=custom-jms:queue:jms/TXN_REL_API_REQ_QUEUE?disableReplyTo=true

### Routing Slip for reject FLA scenario
RejConnectorFile.ContactFile.routingSlip=${RejConnectorFile.CONT_TXFMR.route},\
			${RejConnectorFile.JMS_CONNECTOR.route},\
			${RejConnectorFile.CONT_REJECT.endpoint}	

## Flag to set the request retry waiting time in milisecond. If this is not provided then default time of '86400*1000' ms i.e 1 day will be set automatically
reseqRetryWaitingMs=89400000
## Flag to set the request retry sleep  time in milisecond. If this is not provided then default time of '250' will be set automatically
reseqRetrySleepMs=350


#################################################################################################
### State Management controls for draft and initiate states for unhappy validation scenarios
#################################################################################################
# Flag to indicate if initiate request should be marked as failed (500) if validate call gets timed out. Defaults to false. 
StateMgmt.failInitiateOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if validate call gets timed out during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if there are validation failure (400,422) during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValFailure=true
#################################################################################################

spring.jmx.enabled=false

# CBX to IPSH accepted format (i.e `product#subproduct` to `SUBPRODUCT` )  for PmntMethodAllowed field.
PmntMethodAllowed.cbxToIpshFormat={\
  'paymnt#vdpa01': 'VDPA01', \
  'paymnt#vdpd01': 'VDPD01', \
  'paymnt#vdpi01': 'VDPI01', \
  'paymnt#vdpi11': 'VDPI11', \
  'paymnt#vdpi21': 'VDPI21', \
  'paymnt#pyra01': 'PYRA01', \
  'paymnt#pyra02': 'PYRA02', \
  'paymnt#dvda01': 'DVDA01', \
  'paymnt#dvdd01': 'DVDD01', \
  'paymnt#dvdi01': 'DVDI01', \
  'paymnt#dvdi11': 'DVDI11', \
  'paymnt#dvdi21': 'DVDI21', \
  'paymnt#owna01': 'OWNA01', \
  'paymnt#otha01': 'OTHA01', \
  'paymnt#btra01': 'BTRA01', \
  'paymnt#ttra01': 'TTRA01', \
  'paymnt#dcba01': 'DCBA01', \
  'paymnt#iftads01': 'IFTADS01', \
  'paymnt#iftap101': 'IFTAP101', \
  'paymnt#iftap201': 'IFTAP201', \
  'paymnt#bhtads01': 'BHTADS01', \
  'paymnt#ppsads01': 'PPSADS01', \
  'paymnt#ppnads01': 'PPNADS01', \
  'paymnt#smsads01': 'SMSADS01', \
  'paymnt#smnads01': 'SMNADS01', \
  'paymnt#dftads01': 'DFTADS01', \
  'paymnt#pmtads01': 'PMTADS01', \
  'paymnt#payrl': 'PAYR1', \
  'paymnt#pyra11': 'PYRA11', \
  'paymnt#pyra12': 'PYRA12' \
  }
